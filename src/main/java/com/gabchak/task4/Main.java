package com.gabchak.task4;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static final String PRINT_METHOD_NAME = "print";

    public static void main(String[] args)
            throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException {
        TestClass test = new TestClass("Some text", 10);

        System.out.println(test);
        System.out.println();

        Class<? extends TestClass> textClass = test.getClass();

        Method append = textClass.getDeclaredMethod("append", String.class);
        String otherText = "Other text";
        Object appendResult = append.invoke(test, otherText);
        System.out.printf("Result of method 'append('%s')'\n> %s\n", otherText,
                appendResult);
        System.out.println();

        Method sum = textClass.getDeclaredMethod("sum", Integer.class);
        int intVal = 12;
        Object sumResult = sum.invoke(test, intVal);
        System.out.printf("Result of method 'sum(%d)'\n> %d\n", intVal,
                sumResult);
        System.out.println();

        String newText = "New text";
        intVal = 11;

        Method print = textClass
                .getDeclaredMethod(PRINT_METHOD_NAME, String.class, Integer.class);
        System.out.printf("Invoking 'print('%s', %d)'\n> ", newText, intVal);
        Object result = print.invoke(test, newText, intVal);
        System.out.println("returned: " + result);
        System.out.println();

        print = textClass.getDeclaredMethod(PRINT_METHOD_NAME, String.class);
        System.out.printf("Invoking 'print('%s')'\n> ", newText);
        result = print.invoke(test, newText);
        System.out.println("returned: " + result);
        System.out.println();

        print = textClass.getDeclaredMethod(PRINT_METHOD_NAME, Integer.class);
        System.out.printf("Invoking 'print(%d)'\n> ", intVal);
        result = print.invoke(test, intVal);
        System.out.println("returned: " + result);
        System.out.println();
    }
}