package com.gabchak.task4;

import java.util.StringJoiner;

public class TestClass {
    private String someText;
    private Integer someValue;

    public TestClass(String someText, Integer someValue) {
        this.someText = someText;
        this.someValue = someValue;
    }

    public String append(String text) {
        return someText + " " + text;
    }

    public Integer sum(Integer value) {
        return someValue + value;
    }

    public void print(String text, Integer count) {
        System.out.println(text + ": " + count);
    }

    public String print(String text) {
        System.out.println(text);
        return text;
    }

    public Integer print(Integer count) {
        System.out.println(count);
        return count;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", TestClass.class.getSimpleName() + "[",
                "]").add("someText='" + someText + "'")
                .add("someValue=" + someValue).toString();
    }
}
