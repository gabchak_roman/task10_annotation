package com.gabchak.task6;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args)
            throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException {
        Main main = new Main();

        Method myMethod = main.getClass()
                .getDeclaredMethod("myMethod", String.class, int[].class);
        myMethod.invoke(main, "Text", new int[]{1, 2, 3, 4});


        Method myMethod2 = main.getClass()
                .getDeclaredMethod("myMethod", String[].class);
        myMethod2.invoke(main, new Object[]{new String[]{"param1", "param2", "param3"}});
    }

    public void myMethod(String a, int ... args){
        System.out.print("Passed arguments: " + a);
        for (int arg : args) {
            System.out.print(", " + arg);
        }
        System.out.println();
    }

    public void myMethod(String ... args){
        System.out.print("Passed arguments: " );
        for (String arg : args) {
            System.out.print(", " + arg);
        }
        System.out.println();
    }
}