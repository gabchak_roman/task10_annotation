package com.gabchak.task2;


import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args) throws IllegalAccessException {
        UserDetails details = new UserDetails();
        details.setAddress("Lviv, Shevchenka st. 50");
        details.setCountry("Ukraine");
        details.setName("Oleh");
        details.setRole("Admin");
        details.setAge(21);

        for (Field field : details.getClass().getDeclaredFields()) {
            Printable printable =
                    field.getAnnotation(Printable.class);
            if (printable == null) {
                continue;
            }
            field.setAccessible(true);
            System.out.println(field.getName() + ": " + field.get(details));
        }
    }
}
