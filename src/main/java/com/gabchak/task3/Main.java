package com.gabchak.task3;

public class Main {
    public static final String SOME_FIELD_NAME = "someField";

    public static void main(String[] args) throws NoSuchFieldException {
        TestClass test = new TestClass("some value");
        System.out.println("Annotation value: " + test.getClass()
                .getDeclaredField(SOME_FIELD_NAME).getAnnotation(Annotation.class)
                .name());
    }
}
