package com.gabchak.task7;

import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) {
        SomeClass someClass = new SomeClass("someName", 32);
        OtherClass otherClass = new OtherClass(14f);

        ClassInfo.printClassInfo(someClass);
        ClassInfo.printClassInfo(otherClass);
    }


}