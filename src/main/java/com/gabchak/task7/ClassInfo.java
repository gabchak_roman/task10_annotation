package com.gabchak.task7;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

public class ClassInfo {

    public static final String TAB = "  ";
    public static final String SPACE = " ";
    public static final String EMPTY = "";
    public static final String EMPTY_TAB = EMPTY;

    public static void printClassInfo(Object o) {
        Class<?> oClass = o.getClass();

        System.out.println("-------------------------------------------");
        System.out.println("Class name: " + oClass.getSimpleName());
        System.out.println("package: " + oClass.getPackage());
        System.out.println("class loader: " + oClass.getClassLoader().getClass()
                .getName());

        printAnnotationsInfo(EMPTY_TAB, oClass.getDeclaredAnnotations());

        printFieldsInfo(EMPTY_TAB, oClass.getDeclaredFields());

        printConstuctorsInfo(EMPTY_TAB, oClass.getSimpleName(), oClass.getDeclaredConstructors());

        printMethodsInfo(EMPTY_TAB, oClass.getDeclaredMethods());
    }

    private static void printConstuctorsInfo(String tab, String simpleName,
            Constructor<?>[] constructors) {
        if (constructors.length == 0) {
            return;
        }
        System.out.println(tab + "Constructors:");

        tab += TAB;
        for (Constructor<?> constructor : constructors) {
            printPrettyAnnotations(tab, constructor.getDeclaredAnnotations());
            System.out.println(
                    tab
                            + Modifier.toString(constructor.getModifiers()) + SPACE
                            + simpleName + formatConstructorArguments(constructor));
            System.out.println();
        }
    }

    private static String formatConstructorArguments(Constructor<?> method) {
        StringBuilder b = new StringBuilder("(");
        Parameter[] parameters = method.getParameters();

        if (parameters.length > 0) {
            for (Parameter parameter : parameters) {
                b.append(parameter.getType().getSimpleName()).append(SPACE);
                b.append(parameter.getName());
                b.append(", ");
            }
            b.setLength(b.length() - 2);
        }

        b.append(")");
        return b.toString();
    }

    private static void printMethodsInfo(String tab, Method[] methods) {
        if (methods.length == 0) {
            return;
        }
        System.out.println(tab + "Methods:");

        tab += TAB;
        for (Method method : methods) {
            printPrettyAnnotations(tab, method.getDeclaredAnnotations());
            System.out.println(
                    tab
                        + Modifier.toString(method.getModifiers()) + SPACE
                        + method.getReturnType().getSimpleName() + SPACE
                        + method.getName() + formatMethodArguments(method));
            System.out.println();
        }
    }

    private static String formatMethodArguments(Method method) {
        StringBuilder b = new StringBuilder("(");
        Parameter[] parameters = method.getParameters();

        if (parameters.length > 0) {
            for (Parameter parameter : parameters) {
                b.append(parameter.getType().getSimpleName()).append(SPACE);
                b.append(parameter.getName());
                b.append(", ");
            }
            b.setLength(b.length() - 2);
        }

        b.append(")");
        return b.toString();
    }

    private static void printFieldsInfo(String tab, Field[] fields) {
        if (fields.length == 0) {
            return;
        }
        System.out.println(tab + "Fields:");
        tab += TAB;
        for (Field field : fields) {
            printPrettyAnnotations(tab, field.getDeclaredAnnotations());
            System.out.println(
                    tab + Modifier.toString(field.getModifiers())
                            + SPACE + field.getType().getSimpleName() + SPACE
                            + field.getName());

//            printAnnotationsInfo(tab + TAB, field.getDeclaredAnnotations());
        }
    }

    private static void printAnnotationsInfo(String tab,
            Annotation[] annotations) {
        if (annotations.length == 0) {
            return;
        }
        System.out.println(tab + "Annotations:");
        tab += TAB;
        for (Annotation annotation : annotations) {
            Class<? extends Annotation> aClass = annotation.getClass();
            System.out.println(tab + annotation.annotationType().getName());
            printAnnotationValues(tab, annotation);
        }
    }

    private static void printAnnotationValues(String tab,
            Annotation annotation) {
        Method[] declaredMethods =
                annotation.annotationType().getDeclaredMethods();
        tab += TAB;
        for (Method declaredMethod : declaredMethods) {
            try {
                System.out.println(
                        tab + declaredMethod.getName() + ": " + declaredMethod
                                .invoke(annotation));
            } catch (Exception e) {
            }
        }
    }


    private static void printPrettyAnnotations(String tab,
            Annotation[] annotations) {
        if (annotations.length == 0) {
            return;
        }
        for (Annotation annotation : annotations) {
            Class<? extends Annotation> aClass = annotation.getClass();
            System.out.println(
                    tab + "@" + annotation.annotationType().getSimpleName()
                            + prettyAnnotationValues(annotation));
        }
    }

    private static String prettyAnnotationValues(
            Annotation annotation) {
        Method[] declaredMethods =
                annotation.annotationType().getDeclaredMethods();
        if (declaredMethods.length > 0) {
            StringBuilder b = new StringBuilder("(");
            for (Method declaredMethod : declaredMethods) {
                try {
                    b.append(declaredMethod.getName())
                            .append("='")
                            .append(declaredMethod.invoke(annotation))
                            .append("', ");
                } catch (Exception e) {
                }
            }
            b.setLength(b.length() - 2);
            b.append(")");
            return b.toString();
        }
        return EMPTY;
    }
}
