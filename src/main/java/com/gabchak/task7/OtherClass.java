package com.gabchak.task7;

import java.util.StringJoiner;

public class OtherClass {
    public static Double constant = 34d;

    private Float someFloat;

    public OtherClass(Float someFloat) {
        this.someFloat = someFloat;
    }

    public Float calculate(Integer val) {
        return someFloat + val;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", OtherClass.class.getSimpleName() + "[",
                "]").add("someFloat=" + someFloat).toString();
    }
}
