package com.gabchak.task7;

import com.gabchak.task2.Printable;
import com.gabchak.task3.Annotation;

@Printable
@Annotation(name="Some Class name")
public class SomeClass {
    @Annotation(name="name field")
    private String name;
    @Printable
    public int count;

    @Deprecated
    public SomeClass() {
    }

    public SomeClass(String name, int count) {
        this.name = name;
        this.count = count;
    }

    @Annotation(name="222")
    @Printable
    public String doSomeWork(int number) {
        return "number " + number;
    }

    @Annotation(name="333")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
