package com.gabchak.task5;

import java.util.StringJoiner;

public class UnknownClass {
    private Double unknownField;

    @Override
    public String toString() {
        return new StringJoiner(", ", UnknownClass.class.getSimpleName() + "[",
                "]").add("unknownField=" + unknownField).toString();
    }
}
