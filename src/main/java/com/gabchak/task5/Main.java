package com.gabchak.task5;

import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args)
            throws NoSuchFieldException, IllegalAccessException {
        UnknownClass unknownClass = new UnknownClass();

        Field unknownField =
                unknownClass.getClass().getDeclaredField("unknownField");

        unknownField.setAccessible(true);

        Class<?> type = unknownField.getType();
        System.out.println("Type " + type);
        unknownField.set(unknownClass, 11d);
        System.out.println(unknownClass);
    }
}